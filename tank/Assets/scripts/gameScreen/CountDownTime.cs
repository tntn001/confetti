﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CountDownTime : MonoBehaviour
{
    [SerializeField]
    private Image mCDProgress;

    [SerializeField]
    private GameObject mGroupCdText;

    [SerializeField]
    private Text mTextCountDown;

    private UnityAction<ECountDownCallback> mCallback;

    private bool mIsCountingDown = false;
    private float mCDTime = 0;

    private float mMaxCountDown = 0;    
    private int mCompareStep;

    void OnEnable()
    {
        resetCD();
    }

    void Update()
    {
        if(mIsCountingDown)
        {
            if(mCDTime <=0)
            {
                mCallback.Invoke(ECountDownCallback.FINISH);
                mIsCountingDown = false;
            }
            else
            {
                mCDTime -= Time.deltaTime;
                mCDProgress.fillAmount = mCDTime / mMaxCountDown;
                if (mCDTime<= mCompareStep)
                {
                    mTextCountDown.text = mCompareStep.ToString();                    
                    mCompareStep--;
                }
            }
        }       
    }

    public void beginCD(float time, UnityAction<ECountDownCallback> callback)
    {
        if (!mIsCountingDown)
        {
            mGroupCdText.SetActive(true);
            mCDProgress.fillAmount = 1;
            mTextCountDown.text = time.ToString();
            mCompareStep = (int)time - 1;
            mCDTime = time;
            mMaxCountDown = time;
            mCallback = callback;
            mIsCountingDown = true;
        }
    }

    public void stopCD()
    {
        if (mCallback != null)
        {
            mCallback.Invoke(ECountDownCallback.STOP);
        }
        resetCD();        
    }

    public void resetCD()
    {
        mCompareStep = 0;
        mCDProgress.fillAmount = 0;
        mMaxCountDown = 0;
        mGroupCdText.SetActive(false);
        mIsCountingDown = false;
        mCallback = null;
    }
}

public enum ECountDownCallback
{
    FINISH,
    STOP
}
