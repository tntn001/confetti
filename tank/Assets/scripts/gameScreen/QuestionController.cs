﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionController : MonoBehaviour
{
    [SerializeField]
    private Text mQuestionContent;



    [SerializeField]
    private Text mOptionA;

    [SerializeField]
    private Text mOptionB;

    [SerializeField]
    private Text mOptionC;

    [SerializeField]
    private Text mOptionD;

    [SerializeField]
    private GameObject[] mListBtnOpt;



    private bool mCanChoose = false;

    public void newQuestion(string content)
    {
        mQuestionContent.text = content;

        for (int i = 0; i < mListBtnOpt.Length; i++)
        {
            mListBtnOpt[i].SetActive(true);
        }
    }

    public void preventChoose()
    {
        mCanChoose = false;
    }

    public void reset()
    {
        mQuestionContent.text = "";
        mCanChoose = false;
        for (int i = 0; i < mListBtnOpt.Length; i++)
        {
            mListBtnOpt[i].SetActive(false);
        }
    }


    void OnEnable()
    {
        reset();
    }

    #region UI EVENT

    public void OnClickOptionA()
    {

    }

    public void OnClickOptionB()
    {

    }

    public void OnClickOptionC()
    {

    }

    public void OnClickOptionD()
    {

    }
    #endregion
}

