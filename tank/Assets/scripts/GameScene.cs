﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScene : MonoBehaviour
{
    [SerializeField]
    private CountDownTime mCountDownTimer;

    [SerializeField]
    private QuestionController mQuestionCtrller;

    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Debug.LogError("begin count down");
            mCountDownTimer.beginCD(30, (type) =>
            {
                Debug.LogError(" callback " + type);
                mCountDownTimer.resetCD();
            });
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            mQuestionCtrller.newQuestion(" Hahahahah ???");
        }
    }


    #region UI EVENT
    public void onClickContinue()
    {

    }

    public void onClickNext()
    {

    }

    public void onClickOption(int position)
    {

    }
    #endregion UI EVENT
}
