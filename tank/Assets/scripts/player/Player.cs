﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private const float MAX_SPEED = 0.2f;

    [SerializeField]
    private GamepadBase mGamePad;

    // Start is called before the first frame update
    void Start()
    {
        mGamePad.OnMove += onEventGamepadMove;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void onEventGamepadMove(Vector2 direction, float force)
    {
        var deltaPos = new Vector2(direction.x * force * MAX_SPEED, direction.y * force * MAX_SPEED);
        transform.position = new Vector2(transform.position.x + deltaPos.x, transform.position.y + deltaPos.y);

        var angle = Vector2.Angle(new Vector2(0, 1), direction);

        transform.eulerAngles = new Vector3(0, 0, direction.x < 0 ? angle : -angle);
    }

}
