﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GamepadBase : MonoBehaviour,
    IPointerDownHandler,
    IDragHandler,
    IPointerUpHandler,
    IPointerExitHandler,
    IPointerEnterHandler

{
    public UnityAction<Vector2, float> OnMove;
    public UnityAction<EGamepadButton> OnPressSkill;

    [SerializeField]
    private Canvas mMainCanvas;

    [SerializeField]
    private RectTransform mUIGamepad;

    [SerializeField]
    private RectTransform mUIPointpad;

    private Vector2 mGamepadRealSize;
    private Vector2 mGamepadPosition;
    private Vector2 mVectorDirection;

    private Vector2 mLastCurrentPos;

    private float mRatioVectorDirection;

    private bool mIsEnter = false;
    private bool mIsPressOn = false;
    private bool mIsDrag = false;

    private void OnValidate()
    {
        if (mUIGamepad == null)
        {
            mUIGamepad = GetComponent<RectTransform>();
        }
        if (mUIPointpad == null)
        {
            mUIPointpad = GetComponentsInChildren<RectTransform>()[1];
        }
        if (mMainCanvas == null)
        {
            mMainCanvas = GetComponentInParent<Canvas>();
        }
    }

    private void Start()
    {
        var canvasScale = new Vector2(mMainCanvas.transform.localScale.x, mMainCanvas.transform.localScale.y);

        mGamepadRealSize = new Vector2(mUIGamepad.rect.width * canvasScale.x, mUIGamepad.rect.height * canvasScale.y);

        mGamepadPosition = PivotTransformResolve.GetCenterPosition(mUIGamepad.pivot,
            mUIGamepad.transform.position,
            new Vector2(mUIGamepad.rect.width, mUIGamepad.rect.height),
            canvasScale);

        var maxVector = new Vector2(mGamepadRealSize.x / 2, 0);

        mVectorDirection = maxVector.normalized;

        mRatioVectorDirection = maxVector.magnitude;

    }

    private void Update()
    {
        if (mIsPressOn)
        {
            fireEventOnMove(mLastCurrentPos);
        }
    }


    public void OnDrag(PointerEventData eventData)
    {
        if (mIsPressOn)
        {
            var currentPos = eventData.position;
            if (mIsEnter)
            {
                mUIPointpad.transform.position = currentPos;
                mLastCurrentPos = currentPos;
            }
            else
            {
                var vectorDirection = (new Vector2(currentPos.x - mGamepadPosition.x, currentPos.y - mGamepadPosition.y)).normalized;

                var pos = new Vector2(mGamepadPosition.x + vectorDirection.x * mRatioVectorDirection, mGamepadPosition.y + vectorDirection.y * mRatioVectorDirection);

                mUIPointpad.transform.position = pos;
                mLastCurrentPos = pos;
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        var target = eventData.pointerCurrentRaycast.gameObject;
        if (target != null)
        {
            if (target == mUIGamepad.gameObject)
            {
                mIsPressOn = true;
                mUIPointpad.transform.position = eventData.position;
                mLastCurrentPos = eventData.position;
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (mIsPressOn)
        {
            mIsPressOn = false;
            mUIPointpad.transform.position = mGamepadPosition;
            mLastCurrentPos = mGamepadPosition;
            fireEventOnMove(mGamepadPosition);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (mIsEnter)
        {
            mIsEnter = false;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        var target = eventData.pointerCurrentRaycast.gameObject;
        if (target != null)
        {
            if (target == mUIGamepad.gameObject)
            {
                mIsEnter = true;
            }
        }
    }

    private void fireEventOnMove(Vector2 position)
    {
        if (OnMove != null)
        {
            var direction = new Vector2(position.x - mGamepadPosition.x, position.y - mGamepadPosition.y);
            OnMove.Invoke(direction.normalized, direction.magnitude / mRatioVectorDirection);
        }
    }
}
