﻿using System;
using UnityEngine;

namespace DataClass
{

    [Serializable]
    public class BaseResponse<T>
    {
        [SerializeField]
        public T data;
        [SerializeField]
        public Error[] errors;
        [SerializeField]
        public PaginationData pagination;
    }

    [SerializeField]
    public class Error
    {
        [SerializeField]
        public int errorCode;
        [SerializeField]
        public string errorMessage;
    }

    [Serializable]
    public class PaginationData
    {
        [SerializeField]
        public int totalItems;
        [SerializeField]
        public int pageIndex;
        [SerializeField]
        public int pageSize;
        [SerializeField]
        public int pageCount;
    }



    [Serializable]
    public class Question
    {
        [SerializeField]
        public string id;

        [SerializeField]
        public string content;

        [SerializeField]
        public Option[] options;
    }

    [Serializable]
    public class Option
    {
        [SerializeField]
        public string id;

        [SerializeField]
        public string content;
    }


    [Serializable]
    public class Answer
    {
        [SerializeField]
        public string questionId;
        [SerializeField]
        public string optionId;

        public Answer(string qtId, string optId)
        {
            questionId = qtId;
            optionId = optId;
        }
    }

    [Serializable]
    public class Result
    {
        [SerializeField]
        public string correctAnswerId;
    }
}
