﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;

public struct KeyValue
{
    public string Key
    {
        get;
        private set;
    }
    public string Value
    {
        get;
        private set;
    }
    public KeyValue(string key, string value)
    {
        Key = key;
        Value = value;
    }
}


public sealed class ApiHelper
{
    private const string _BASE_URL = "https://jsonplaceholder.typicode.com/users";
    private const int _TIME_OUT_ = 10;


    private static readonly Lazy<ApiHelper> mLazy = new Lazy<ApiHelper>(() => new ApiHelper());

    private Dictionary<string, string> mGetHeader;
    private Dictionary<string, string> mPostHeader;

    public static ApiHelper Instance
    {
        get
        {
            return mLazy.Value;
        }
    }

    private ApiHelper()
    {
        mGetHeader = new Dictionary<string, string>();
        mGetHeader.Add("Authorization", "");

        mPostHeader = new Dictionary<string, string>();
        mPostHeader.Add("Authorization", "");
        mPostHeader.Add("Content-Type", "application/json");
    }

    private RequestHelper helperGet(string url)
    {
        var helper = new RequestHelper();
        helper.Uri = _BASE_URL + url;
        helper.Timeout = _TIME_OUT_;
        helper.Method = "GET";
        return helper;
    }
    private RequestHelper helperPost(string url, params KeyValue[] multiKeyValue)
    {
        var helper = new RequestHelper();
        helper.Uri = _BASE_URL + url;
        helper.Timeout = _TIME_OUT_;
        helper.Method = "POST";
        if (multiKeyValue.Length > 0)
        {
            var headers = new Dictionary<string, string>();
            for (int i = 0; i < multiKeyValue.Length; i++)
            {
                headers.Add(multiKeyValue[i].Key, multiKeyValue[i].Value);
            }
            helper.Headers = headers;
        }
        return helper;
    }

    public void getQuestion(Action<DataClass.Question> callback)
    {
        RestClient.Request(helperGet("")).Then(response =>
        {
            Debug.LogError(response.Text);
            callback.Invoke(JsonUtility.FromJson<DataClass.Question>(response.Text));

        }).Catch(error =>
        {

        });
    }

    public void postAnswer(string questionId, string optionId, Action<DataClass.Result> callback)
    {
        var header = helperPost("");
        header.Body = new DataClass.Answer(questionId, optionId);
        RestClient.Request(header).Then(response =>
        {
            Debug.LogError(response.Text);
            callback.Invoke(JsonUtility.FromJson<DataClass.Result>(response.Text));

        }).Catch(error =>
        {

        });
    }
}