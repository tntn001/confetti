﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PivotTransformResolve
{
    public static Vector2 GetCenterPosition(Vector2 pivot, Vector2 currentPos, Vector2 size, Vector2 canvasScale)
    {
        var deltaPivot = new Vector2(0.5f - pivot.x, 0.5f - pivot.y);

        var realSize = new Vector2(size.x * canvasScale.x, size.y * canvasScale.y);

        var pos = new Vector2(currentPos.x + realSize.x * deltaPivot.x, currentPos.y + realSize.y * deltaPivot.y);

        return pos;
    }
}
